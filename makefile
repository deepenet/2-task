CC = g++

CFlags = -O3 -std=c++17

all: build

build: main.o stackCalc.o
	$(CC) main.o stackCalc.o -o main

main.o: main.cpp
	$(CC) $(CFlags) main.cpp -c

stackCalc.o: stackCalc.cpp stackCalc.h
	$(CC) $(CFlags) stackCalc.cpp -c	

clear:
	rm *.o