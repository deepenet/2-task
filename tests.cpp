#include "stackCalc.cpp"
#include <gtest/gtest.h>

TEST(executionContext, stack) {
	executionContext test{};
	void push(double value);
	//pop from stack
	void pop();
	//return last element of stack
	double showBack();
	//check stack empty
	bool empty();
	//return stack size
	size_t size();
	test.push(0);
	test.push(1);
	test.push(-1);
	test.push(1e100);
	test.push(0.15);
	EXPECT_EQ(0.15, test.showBack());
	EXPECT_EQ(5, test.size());
	EXPECT_EQ(false, test.empty());
	test.pop();
	EXPECT_EQ(1e100, test.showBack());
	test.pop();
	EXPECT_EQ(-1, test.showBack());
	test.pop();
	EXPECT_EQ(1, test.showBack());
	test.pop();
	EXPECT_EQ(0, test.showBack());
	test.pop();
	EXPECT_EQ(true, test.empty());
}

TEST(executionContext, define) {
	executionContext test{};
	test.define("name", 1.5);
	test.define("super_rea11y_10ng_name_with_numbers", 1.123e50);
	EXPECT_EQ(1.5, test.find("name"));
	EXPECT_EQ(1.123e50, test.find("super_rea11y_10ng_name_with_numbers"));
}

TEST(operation, push_pop_print) {
	executionContext test{};
	PUSH psh(&test);
	POP pp(&test);
	PRINT prnt(&test);
	std::list<std::string> parametersList;
	//bad parameters list
	ASSERT_THROW(psh.perform(parametersList), badOperationParameters);
	parametersList.push_back("1");
	ASSERT_THROW(pp.perform(parametersList), badOperationParameters);
	ASSERT_THROW(prnt.perform(parametersList), badOperationParameters);
	parametersList.push_back("2");
	ASSERT_THROW(psh.perform(parametersList), badOperationParameters);
	ASSERT_THROW(pp.perform(parametersList), badOperationParameters);
	ASSERT_THROW(prnt.perform(parametersList), badOperationParameters);
	while(!parametersList.empty()) {
		parametersList.pop_back();
	}
	parametersList.push_back("1");
	psh.perform(parametersList);
	parametersList.push_back("2");
	psh.perform(parametersList);
	EXPECT_EQ(2, test.showBack());
	prnt.perform(parametersList);
	EXPECT_EQ(2, test.showBack());
	pp.perform(parametersList);
	EXPECT_EQ(1, test.showBack());
	pp.perform(parametersList);
}

TEST(operation, sum) {
	executionContext test{};
	SUM a(&test);
	std::list<std::string> parametersList;
	ASSERT_THROW(a.perform(parametersList), stackEmpty);
	test.push(1);
	ASSERT_THROW(a.perform(parametersList), notEnoughSizeStack);
	test.push(-1);
	a.perform(parametersList);
	EXPECT_EQ(0, test.showBack());
	test.push(std::atof("1.3e-13"));
	a.perform(parametersList);
	EXPECT_EQ(std::atof("1.3e-13"), test.showBack());
	test.push(std::atof("1e500"));
	a.perform(parametersList);
	EXPECT_EQ(std::atof("inf"), test.showBack());
}

TEST(operation, sub) {
	executionContext test{};
	SUB a(&test);
	std::list<std::string> parametersList;
	ASSERT_THROW(a.perform(parametersList), stackEmpty);
	test.push(-1);
	ASSERT_THROW(a.perform(parametersList), notEnoughSizeStack);
	test.push(-1);
	a.perform(parametersList);
	EXPECT_EQ(0, test.showBack());
	test.push(0.5);
	test.push(0.25);
	a.perform(parametersList);
	EXPECT_EQ(-0.25, test.showBack());
	test.push(-0.25);
	a.perform(parametersList);
	EXPECT_EQ(0, test.showBack());
	test.push(std::atof("1.3e-13"));
	a.perform(parametersList);
	EXPECT_EQ(std::atof("1.3e-13"), test.showBack());
	test.push(std::atof("1e500"));
	a.perform(parametersList);
	EXPECT_EQ(std::atof("inf"), test.showBack());
}

TEST(operation, mult) {
	executionContext test{};
	MULT a(&test);
	std::list<std::string> parametersList;
	ASSERT_THROW(a.perform(parametersList), stackEmpty);
	test.push(-1);
	ASSERT_THROW(a.perform(parametersList), notEnoughSizeStack);
	test.push(-1);
	a.perform(parametersList);
	EXPECT_EQ(1, test.showBack());
	test.push(0.5);
	test.push(0.25);
	a.perform(parametersList);
	a.perform(parametersList);
	EXPECT_EQ(0.125, test.showBack());
	test.push(0);
	a.perform(parametersList);
	EXPECT_EQ(0, test.showBack());
	test.push(std::atof("1.3e-13"));
	a.perform(parametersList);
	EXPECT_EQ(0, test.showBack());
}

TEST(operation, div) {
	executionContext test{};
	DIV a(&test);
	std::list<std::string> parametersList;
	ASSERT_THROW(a.perform(parametersList), stackEmpty);
	test.push(-1);
	ASSERT_THROW(a.perform(parametersList), notEnoughSizeStack);
	test.push(-1);
	a.perform(parametersList);
	EXPECT_EQ(1, test.showBack());
	test.push(0.5);
	test.push(0.25);
	a.perform(parametersList);
	a.perform(parametersList);
	EXPECT_EQ(0.5, test.showBack());
	test.push(0);
	a.perform(parametersList);
	EXPECT_EQ(0, test.showBack());
	test.push(std::atof("1.3e-13"));
	a.perform(parametersList);
	EXPECT_EQ(std::atof("inf"), test.showBack());
}

TEST(operation, sqrt) {
	executionContext test{};
	SQRT a(&test);
	std::list<std::string> parametersList;
	ASSERT_THROW(a.perform(parametersList), stackEmpty);
	test.push(1);
	a.perform(parametersList);
	EXPECT_EQ(1, test.showBack());
	test.push(0.25);
	a.perform(parametersList);
	EXPECT_EQ(0.5, test.showBack());
	test.push(0);
	a.perform(parametersList);
	EXPECT_EQ(0, test.showBack());
	test.push(std::atof("1.3e-13"));
	a.perform(parametersList);
	EXPECT_EQ(std::atof("3.6055512754639892931192212674705e-7"), test.showBack());
}

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}