#pragma once
#include <iostream>
#include <vector>
#include <string>
#include <map>
#include <cmath>
#include <exception>
#include <cctype>
#include <list>
#include <fstream>

class badRunParameters : public std::exception {
	const char* what() const noexcept;
};

class cannotOpenFile : public std::exception {
	const char* what() const noexcept;
};

class invalidDefine : public std::exception {
	const char* what() const noexcept;
};

class invalidPushingData : public std::exception {
	const char* what() const noexcept;
};

class defineNotFound : public std::exception {
	const char* what() const noexcept;
};

class badOperationParameters : public std::exception {
	const char* what() const noexcept;
};

class notEnoughSizeStack : public std::exception {
	const char* what() const noexcept;
};

class badOperationName : public std::exception {
	const char* what() const noexcept;
};

class stackEmpty : public std::exception {
	const char* what() const noexcept;
};

class executionContext {
	std::vector<double> stack;
	std::map<std::string, double> definitions;
public:
	//push to stack
	void push(double value);
	//pop from stack
	void pop();
	//return last element of stack
	double showBack();
	//check stack empty
	bool empty();
	//return stack size
	size_t size();
	//put definition on map
	void define(std::string name, double value);
	//find definition in map
	double find(std::string name);
};

//base class for any input commands
class operation {
protected:
	executionContext *execContext;
public:
	operation(executionContext *data);
	//do action wich class should
	virtual void perform(std::list<std::string> &) = 0;
};

//class for # - comment
class COMMENT: public operation {
public:
	COMMENT(executionContext *data) : operation(data) {}
	//ignore anything
	void perform(std::list<std::string> &);
};

class SUM: public operation {
public:
	SUM(executionContext *data) : operation(data) {}
	void perform(std::list<std::string> &parametersList);
};

class SUB: public operation {
public:
	SUB(executionContext *data) : operation(data) {}
	void perform(std::list<std::string> &parametersList);
};

class MULT: public operation {
public:
	MULT(executionContext *data) : operation(data) {}
	void perform(std::list<std::string> &parametersList);
};

class DIV: public operation {
public:
	DIV(executionContext *data) : operation(data) {}
	void perform(std::list<std::string> &parametersList);
};

class SQRT: public operation {
public:
	SQRT(executionContext *data) : operation(data) {}
	void perform(std::list<std::string> &parametersList);
};

class PRINT: public operation {
public:
	PRINT(executionContext *data) : operation(data) {}
	void perform(std::list<std::string> &parametersList);
};

class PUSH: public operation {
public:
	PUSH(executionContext *data) : operation(data) {}
	void perform(std::list<std::string> &parametersList);
};

class POP: public operation {
public:
	POP(executionContext *data) : operation(data) {}
	void perform(std::list<std::string> &parametersList);
};

class DEFINE: public operation {
public:
	DEFINE(executionContext *data) : operation(data) {}
	void perform(std::list<std::string> &parametersList);
};

//base class of each factory
class Factory {
protected:
	executionContext *execContext;
public:    
	//create object of operation class
	virtual operation* createOperation(executionContext *data) = 0;
	virtual ~Factory() {}
};

class commentFactory: public Factory {
public:    
	operation* createOperation(executionContext *data);
};

class sumFactory: public Factory {
public:    
	operation* createOperation(executionContext *data);
};

class subFactory: public Factory {
public:    
	operation* createOperation(executionContext *data);
};

class multFactory: public Factory {
public:    
	operation* createOperation(executionContext *data);
};

class divFactory: public Factory {
public:    
	operation* createOperation(executionContext *data);
};

class sqrtFactory: public Factory {
public:    
	operation* createOperation(executionContext *data);
};

class pushFactory: public Factory {
public:    
	operation* createOperation(executionContext *data);
};

class popFactory: public Factory {
public:    
	operation* createOperation(executionContext *data);
};

class printFactory: public Factory {
public:    
	operation* createOperation(executionContext *data);
};

class defineFactory: public Factory {
public:    
	operation* createOperation(executionContext *data);
};

//class calculator makes all work
class calculator {
		std::ifstream fin;
		bool isstdstream = false;
		executionContext stackDefineData;
		std::map<std::string, Factory*> operations;
		std::string line = "";
public:
	//run calculator
	void run(std::string nameFile = "");
};