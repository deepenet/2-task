#include "stackCalc.h"

const char* badRunParameters::what() const noexcept {
	return "ERROR: Wrong arguments of command line\n";
}

const char* cannotOpenFile::what() const noexcept {
	return "ERROR: Cannot open file\n";
}

const char* invalidDefine::what() const noexcept {
	return "ERROR: Invalid definition\n";
}

const char* invalidPushingData::what() const noexcept {
	return "ERROR: Invalid push parameters\n";
}

const char* defineNotFound::what() const noexcept {
	return "ERROR: Definition not found\n";
}

const char* badOperationParameters::what() const noexcept {
	return "ERROR: Wrong number of operation parameters\n";
}

const char* notEnoughSizeStack::what() const noexcept {
	return "ERROR: Not enough number of elements of stack\n";
}

const char* badOperationName::what() const noexcept {
	return "ERROR: Bad operation name\n";
}

const char* stackEmpty::what() const noexcept {
	return "ERROR: Stack empty\n";
}

void executionContext::push(double value) {
	stack.push_back(value);
}
void executionContext::pop() {
	stack.pop_back();
}
double executionContext::showBack() {
	if(stack.empty())
		throw stackEmpty{};
	return stack.back();
}
bool executionContext::empty() {
	return stack.empty();
}
size_t executionContext::size() {
	return stack.size();
}
void executionContext::define(std::string name, double value) {
	definitions[name] = value;
}
double executionContext::find(std::string name) {
	auto ptr = definitions.find(name);
	if(ptr == definitions.end())
		throw defineNotFound{};
	return ptr->second; 
}

operation::operation(executionContext *data) {
	execContext = data;
}

void COMMENT::perform(std::list<std::string> &) {}

void SUM::perform(std::list<std::string> &parametersList) {
	if(execContext->empty())
		throw stackEmpty{};
	if(execContext->size() < 2) {
		throw notEnoughSizeStack{};
	}
	if(parametersList.size() > 0) {
		throw badOperationParameters{};
	}
	double first = execContext->showBack();
	execContext->pop();
	double second = execContext->showBack();
	execContext->pop();
	execContext->push(first + second);
}

void SUB::perform(std::list<std::string> &parametersList) {
	if(execContext->empty())
		throw stackEmpty{};
	if(execContext->size() < 2) {
		throw notEnoughSizeStack{};
	}
	if(parametersList.size() > 0) {
		throw badOperationParameters{};
	}
	double first = execContext->showBack();
	execContext->pop();
	double second = execContext->showBack();
	execContext->pop();
	execContext->push(first - second);
}

void MULT::perform(std::list<std::string> &parametersList) {
	if(execContext->empty())
		throw stackEmpty{};
	if(execContext->size() < 2) {
		throw notEnoughSizeStack{};
	}
	if(parametersList.size() > 0) {
		throw badOperationParameters{};
	}
	double first = execContext->showBack();
	execContext->pop();
	double second = execContext->showBack();
	execContext->pop();
	execContext->push(first * second);
}

void DIV::perform(std::list<std::string> &parametersList) {
	if(execContext->empty())
		throw stackEmpty{};
	if(execContext->size() < 2) {
		throw notEnoughSizeStack{};
	}
	if(parametersList.size() > 0) {
		throw badOperationParameters{};
	}
	double first = execContext->showBack();
	execContext->pop();
	double second = execContext->showBack();
	execContext->pop();
	execContext->push(first / second);
}

void SQRT::perform(std::list<std::string> &parametersList) {
	if(execContext->empty())
		throw stackEmpty{};
	if(parametersList.size() > 0) {
		throw badOperationParameters{};
	}
	double first = execContext->showBack();
	execContext->pop();
	execContext->push(std::sqrt(first));
}

void PRINT::perform(std::list<std::string> &parametersList) {
	if(parametersList.size() > 0) {
		throw badOperationParameters{};
	} else if(execContext->empty()) {
		std::cout << "Empty\n";
	} else {
		double first = execContext->showBack();
		std::cout << first << '\n';
	}
}

void PUSH::perform(std::list<std::string> &parametersList) {
	if(!parametersList.size() || parametersList.front() == "")
		throw badOperationParameters{};
	std::string name = parametersList.front();
	parametersList.pop_front();
	if(parametersList.size() > 0)
		throw badOperationParameters{};
	bool flagNumber = true;
	int numbPoints = 0;
	int numbE = 0;
	bool pointBeforeE = true;
	bool canBeName = true;
	double value;
	//check if name can be double number
	for(int i = 0; i < name.size(); i++) {
		if(isalpha(name[i]) || isdigit(name[i]) || name[i] == '.' || name[i] == '-') {
			if(isdigit(name[i]) || name[i] == '.' || name[i] == 'e' || name[i] == '-') {
				if(name[i] == '.') {
					if(numbE > 0) {
						pointBeforeE = false;
					}
					numbPoints++;
				} else if(name[i] == 'e') {
					numbE++;
				}
			} else {
				flagNumber = false;
			}
		} else {
			canBeName = false;
		}
	}
	//if double came
	if(flagNumber && numbPoints < 2 && numbE < 2 && pointBeforeE) {
		value = std::atof(name.c_str());
	}
	//if name came
	else {
		if(!canBeName || isdigit(name[0])) {
			throw invalidPushingData{};
		}
		value = execContext->find(name);
	}
	execContext->push(value);
}

void POP::perform(std::list<std::string> &parametersList) {
	if(parametersList.size() > 0) {
		throw badOperationParameters{};
	} 
	if(execContext->empty())
		throw stackEmpty{};
	execContext->pop();
}

void DEFINE::perform(std::list<std::string> &parametersList) {
	//start check correct input
	if(parametersList.size() < 2 || parametersList.front() == "") {
		throw badOperationParameters{};
	}
	std::string name = parametersList.front();
	parametersList.pop_front();
	if(parametersList.front() == "") {
		throw badOperationParameters{};
	}
	std::string number = parametersList.front();
	parametersList.pop_front();
	if(parametersList.size() > 0) {

	}
	bool canBeName = true;
	bool canBeNumb = true;
	bool pointBeforeE = true;
	int numbPoints = 0;
	int numbE = 0;
	for(int i = 0; i < name.size(); i++) {
		if(!(isalpha(name[i]) || isdigit(name[i]) || name[i] == '.' || name[i] == '-')) {
			canBeName = false;
		}
	}
	for(int i = 0; i < number.size(); i++) {
		if(isdigit(number[i]) || number[i] == '.' || number[i] == 'e' || number[i] == '-') {
			if(number[i] == '.') {
				if(numbE > 0) {
					pointBeforeE = false;
				}
				numbPoints++;
			} else if(number[i] == 'e') {
				numbE++;
			}
		} else {
			canBeNumb = false;
		}
	}
	if(!canBeName || isdigit(name[0]) || !canBeNumb || !pointBeforeE || numbPoints > 1 || numbE > 1) {
		throw invalidDefine{};
	}
	//stop check
	execContext->define(name, std::atof(number.c_str()));
}

operation* commentFactory::createOperation(executionContext *data) { 
	return new COMMENT(data); 
}

operation* sumFactory::createOperation(executionContext *data) { 
	return new SUM(data); 
}

operation* subFactory::createOperation(executionContext *data) { 
	return new SUB(data); 
}

operation* multFactory::createOperation(executionContext *data) { 
	return new MULT(data); 
}

operation* divFactory::createOperation(executionContext *data) { 
	return new DIV(data); 
}

operation* sqrtFactory::createOperation(executionContext *data) { 
	return new SQRT(data); 
}

operation* pushFactory::createOperation(executionContext *data) { 
	return new PUSH(data); 
}

operation* popFactory::createOperation(executionContext *data) { 
	return new POP(data); 
}

operation* printFactory::createOperation(executionContext *data) { 
	return new PRINT(data); 
}

operation* defineFactory::createOperation(executionContext *data) { 
	return new DEFINE(data); 
}

void calculator::run(std::string nameFile) {
	if(nameFile != "") {
		fin.open(nameFile, std::fstream::in);
		if(!fin) throw cannotOpenFile{};
	} else {
		isstdstream = true;
	}
	operations["#"] = new commentFactory{};
	operations["PUSH"] = new pushFactory{};
	operations["POP"] = new popFactory{};
	operations["+"] = new sumFactory{};
	operations["-"] = new subFactory{};
	operations["*"] = new multFactory{};
	operations["/"] = new divFactory{};
	operations["SQRT"] = new sqrtFactory{};
	operations["PRINT"] = new printFactory{};
	operations["DEFINE"] = new defineFactory{};
	while(std::getline(isstdstream ? std::cin : fin, line)) {
		try {
			int counter = 0;
			int tmp = 0;
			std::list<std::string> parametersList;
			std::string tmpStr = "";
			while(true) {
				counter++;
				for(int i = 0; tmp < line.size() && !isspace(line[tmp]); ++tmp, ++i) {
					tmpStr += line[tmp];
				}
				if(tmpStr != "")
					parametersList.push_back(tmpStr);
				tmpStr = "";
				if(tmp >= line.size()) {
					break;
				}
				tmp++;
			}
			auto ptr = operations.find(parametersList.front());
			parametersList.pop_front();
			if(ptr == operations.end()) {
				throw badOperationName{};
			}
			operation* action = ptr->second->createOperation(&stackDefineData);
			action->perform(parametersList);
			delete action;
		}
		catch(const std::exception &except) {
			std::cout << except.what();
		}
	}
	delete operations["#"];
	delete operations["PUSH"];
	delete operations["POP"];
	delete operations["+"];
	delete operations["-"];
	delete operations["*"];
	delete operations["/"];
	delete operations["SQRT"];
	delete operations["PRINT"];
	delete operations["DEFINE"];
}