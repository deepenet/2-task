#include <iostream>
#include "stackCalc.h"

int main(int argc, char* argv[]) {
	calculator calc{};
	try {
		if(argc == 1) {
			calc.run();
		}
		else if(argc == 2) {
			calc.run(argv[1]);
		}
		else throw badRunParameters{};
	}
	catch(std::exception &except) {
		std::cout << except.what();
	}
	return 0;
}